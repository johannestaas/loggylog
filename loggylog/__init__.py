'''
loggylog

Complex logging, simple API
'''

from .logger import Logger

__title__ = 'loggylog'
__version__ = '0.0.3'
__all__ = ('Logger',)
__author__ = 'Johan Nestaas <johannestaas@gmail.com>'
__license__ = 'GPLv3+'
__copyright__ = 'Copyright 2016 Johan Nestaas'
